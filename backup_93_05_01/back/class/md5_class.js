var crypto = require('crypto');
module.exports.md5 = md5;
function md5(name)
{
	var hash = crypto.createHash('md5').update(name).digest('hex');
	return(hash);
}

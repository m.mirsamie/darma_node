var dirSep = "/";
var mysql = require('mysql');
var mainPath = loadpath();//'/home/mehrdad_mahsa/projects/node/behdani';
//var mainPath = '/root/behdani';
var resToCl = require(mainPath+'/tcp_new.js').resToCl;
var addLog = require(mainPath+'/tcp_new.js').addLog;
var getPool = require(mainPath+'/tcp_new.js').getPool;
var poolDB = getPool();
module.exports.user_etebar_class = user_etebar_class;
function loadpath()
{
	var out = '';
	var mainPath = __dirname;
	//mainPath = 'c:\\ali\\gholi\\class';
	out = mainPath.replace(/\//g,"|");
	out = out.replace(/\\/g,"|");
	var tmp = String(out).split("|");
	var out = "";
	for(var i = 0;i < tmp.length;i++)
		if(tmp[i]!='')
			if((i == tmp.length-1 && tmp[i]!='class')||(i < tmp.length-1))
				out += ((out!='' || mainPath.charAt(0)==dirSep)?dirSep:'')+tmp[i];
	return out;
}
function user_etebar_class(){
	this.id=-1;
	this.user_id=-1;
	this.typ=-1;
	this.mablagh=0;
	this.toz="";
	this.regdate;
	this.transaction_id=-1;
	this.err;
	this.construct = function (id,connection,fn)//inHost,inPort,inDatabase,inUser,inPassword,fn)
	{

		this.connection = mysql.createConnection({
			host : connection.config.host,//'localhost',
			port : connection.config.port,//3306,
			database: poolDB,//'behdani',
			user : connection.config.user,//'root',
			password : connection.config.password,//'123456'
		});

//		this.connection = connection;
		if(typeof fn=='function')
			fn(this);
	};
	this.add = function (fn)
	{
		var paren = this;
		var con = this.connection;
		this.connection.query("select company_id from user where id = "+paren.user_id,function(err,rows){
			if(err)
			{
			}
			else
			{
				var company_id = rows[0].company_id;
				con.query("insert into user_etebar (`parvande_profile_id`,`typ`,`mablagh`,`toz`,`regdate`,`transaction_id`,`company_id`) values ("+paren.user_id+","+paren.typ+","+paren.mablagh+",'"+paren.toz+"','"+paren.regdate+"',"+paren.transaction_id+","+company_id+")",function(err,rows){
					if(err)
						paren.err = err;
					else
						paren.id = rows.insertId;
					if(typeof fn == 'function')
						fn(paren);
				});
			}
		});
	};
	this.user_etebar_fn = function(cards,dataObj,funcCode,ter,tran,typ,socket,isEnteghal,fnfn)
	{
		var user_etebar = this;
		var terminal_id = ter.id;
		var fname = ter.fname;
		var lname = ter.lname
		var user_id = cards.user_id;
		var con = this.connection;
		user_etebar.user_id = user_id;
		user_etebar.mablagh = dataObj[2];
		user_etebar.toz = ((typ>0)?"افزایش":"کاهش")+" اعتبار "+dataObj[2]+" ریال بابت "+((!isEnteghal && typ>-2)?" خرید از کارتخوان شماره "+terminal_id+" به نام " +fname+" "+lname:" انتقال ")+((typ>0)?' به ':' از ')+"  کارت شماره"+cards.shomare;
		user_etebar.typ=(typ<0)?-1:1;
		user_etebar.regdate = tran.tarikh;
		user_etebar.transaction_id = tran.id;
		user_etebar.add(function(inp){
			user_etebar = inp;
			if(user_etebar.err)
			{
				stat=4;
				addLog(user_id,"درخواست خرید مشکل در ثبت اعتبار اکشن","user etebar add unsuccessfull ,user_etebar.add,stat="+stat);
				resToCl(terminal_id,funcCode,socket,stat,dataObj[0]);
			}
			else
			{
				cards.decEtebar(dataObj[2],isEnteghal,function(inp){
					if(inp)
					{
						stat=4;
						addLog(user_id,"درخواست خرید مشکل در کاهش اعتبار اکشن","user etebar add unsuccessfull ,cards.decEtebar,stat="+stat);
						resToCl(terminal_id,funcCode,socket,stat,dataObj[0]);
					}
					else
					{
						if((typeof fnfn == 'function') || typ == -1)
						{
							var qqqq = " last_trans_kharid = ";
							var qq23 = "";
							if((typeof fnfn == 'function'))
								qqqq = " last_trans_enteghal = ";
							if(parseInt(cards.saghf_hadie,10)>0)
							{
								qq23 = " , saghf_hadie = saghf_hadie - "+String(dataObj[2]);
								cards.addMinLimit(dataObj[2]);
							}
							con.query("update cards set "+qqqq+" "+tran.id+qq23+" where id = "+cards.id,function(err,res){
								if(err)
								{
									stat=4;
									addLog(user_id,"درخواست خرید مشکل در ذخیره شماره تراکنش کارت "+cards.shomare,"user etebar add unsuccessfull ,cards.decEtebar update last transaction,stat="+stat);
									resToCl(terminal_id,funcCode,socket,stat,dataObj[0]);
								}
								else
								{
									stat="1,"+tran.id;
									addLog(user_id,user_etebar.toz,"user eteabr add successfull ,user_etebar.add,stat="+stat);
									if(typeof fnfn == 'function')
										fnfn(true);
									else
										resToCl(terminal_id,funcCode,socket,stat,dataObj[0]);
								}
							});
						}
						else
						{
							stat="1,"+tran.id;
							addLog(user_id,user_etebar.toz,"user eteabr "+((isEnteghal)?'transfer':'add')+" successfull ,user_etebar.add,stat="+stat);
							if(typeof fnfn == 'function')
								fnfn(true);
							else
								resToCl(terminal_id,funcCode,socket,stat,dataObj[0]);
						}
					}
				});
			}
		});				

	};
}

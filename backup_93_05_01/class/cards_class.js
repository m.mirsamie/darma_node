var dirSep = "/";
var mysql = require('mysql');
var mainPath = loadpath();//'/home/mehrdad_mahsa/projects/node/behdani';
//var mainPath = '/root/behdani';
var resToCl = require(mainPath+'/tcp_new.js').resToCl;
var addLog = require(mainPath+'/tcp_new.js').addLog;
var md5 = require(mainPath+'/class/md5_class.js').md5;
var getPool = require(mainPath+'/tcp_new.js').getPool;
var poolDB = getPool();
module.exports.cards_class = cards_class;
function loadpath()
{
	var out = '';
	var mainPath = __dirname;
	out = mainPath.replace(/\//g,"|");
	out = out.replace(/\\/g,"|");
	var tmp = String(out).split("|");
	var out = "";
	for(var i = 0;i < tmp.length;i++)
		if(tmp[i]!='')
			if((i == tmp.length-1 && tmp[i]!='class')||(i < tmp.length-1))
				out += ((out!='' || mainPath.charAt(0)==dirSep)?dirSep:'')+tmp[i];
	return out;
}
function cards_class(){
	this.id=-1;
	this.rfid=-1;
	this.user_id=-1;
	this.tarikh_sodor;
	this.shomare='';
	this.last_trans_kharid=-1;
	this.last_trans_enteghal=-1;
	this.expire_date;
	this.pass='';
	this.passr='';
	this.wrong_count=0;
	this.wrong_date;
	this.en=1;
	this.saghf_rooz=1000000;
	this.saghf_hadie=-1;
	this.saghf_enteghal=10000000;
	this.karmozd=2000;
	this.rooz_mablagh=0;
	this.rooz_enteghal=0;
	this.etebar = 0;
	this.min_etebar = 0;
	this.fname = "";
	this.lname = "";
	this.err;
	this.construct = function (id,connection,fn)
	{
		var def={};
	        def.id=-1;
		def.rfid=-1;
		def.user_id=-1;
		def.tarikh_sodor="0000-00-00 00:00:00";
		def.shomare='';
		def.last_trans_kharid=-1;
		def.last_trans_enteghal =-1;
		def.expire_date="0000-00-00 00:00:00";
		def.pass='';
		def.passr='';
		def.wrong_count=0;
		def.wrong_date="0000-00-00 00:00:00";
		def.en=1;
		def.saghf_rooz=1000000;
		def.saghf_hadie=-1;
		def.saghf_enteghal=10000000;
		def.karmozd=2000;
		def.rooz_mablagh=0;
		def.rooz_enteghal=0;
		def.etebar = 0;
                def.min_etebar = 0;
		def.fname = "";
		def.lname = "";
		def.err=null;
		this.connection = mysql.createConnection({
			host : connection.config.host,//'localhost',
			port : connection.config.port,//3306,
			database: poolDB,//'behdani',
			user : connection.config.user,//'root',
			password : connection.config.password,//'123456'
		});
		var paren = this;
		if(typeof id != 'undefined')
		{
			var con = connection;
			this.connection.query("select * from cards where rfid='"+id+"'" ,function(err,rows){
				if(err)
				{
					paren.err = err;
					if(typeof fn == 'function')
						fn(paren);
				}
				else if(rows.length>0)
				{
					var tmp = rows[0];
					for(i in tmp)
						if(typeof i != 'function')
							paren[i] = tmp[i];
					var qtt = "select etebar,fname,lname,min_etebar from naghshe.parvande left join naghshe.user_parvande on (user_parvande.parvande_id=parvande.id) where user_parvande.user_id = "+paren.user_id;
					if(paren.company_id==2)
						qtt = "select etebar,fname,lname,min_etebar from darma.profile left join "+poolDB+".user on (user.id=user_id) where user_id = "+paren.user_id;
					con.query(qtt,function(err,rows){
						if(err)
							paren.err = err;
						else if(rows.length > 0)
						{
							paren.etebar = rows[0].etebar;
							paren.fname = rows[0].fname;
							paren.lname = rows[0].lname;
							paren.min_etebar = rows[0].min_etebar;
						}
						else
							paren.etebar = 0;
						if(typeof fn == 'function')
							fn(paren);
						
					});
				}
				else
				{
					for(i in def)
						if(typeof i != 'function')
							paren[i] = def[i];
					if(typeof fn == 'function')
						fn(paren);
				}
			});
		}
	};
	this.loadByShomare = function(shomare,connection,fn)
	{
		var def={};
	        def.id=-1;
		def.rfid=-1;
		def.user_id=-1;
		def.tarikh_sodor="0000-00-00 00:00:00";
		def.shomare='';
		def.last_trans=-1;
		def.expire_date="0000-00-00 00:00:00";
		def.pass='';
		def.passr='';
		def.wrong_count=0;
		def.wrong_date="0000-00-00 00:00:00";
		def.en=1;
		def.saghf_rooz=1000000;
		def.saghf_hadie=-1;
		def.saghf_enteghal=10000000;
		def.karmozd=2000;
		def.rooz_mablagh=0;
		def.rooz_enteghal=0;
		def.etebar = 0;
                def.min_etebar = 0;
		def.fname = "";
		def.lname = "";
		def.err=null;
		this.connection = mysql.createConnection({
			host : connection.config.host,//'localhost',
			port : connection.config.port,//3306,
			database: poolDB,//'behdani',
			user : connection.config.user,//'root',
			password : connection.config.password,//'123456'
		});
		var paren = this;
		if(typeof shomare != 'undefined')
		{
			var con = paren.connection;
			this.connection.query("select * from "+poolDB+".cards where shomare="+shomare ,function(err,rows){
				if(err)
				{
					paren.err = err;
					if(typeof fn == 'function')
						fn(paren);
				}
				else if(rows.length>0)
				{
					var tmp = rows[0];
					for(i in tmp)
						if(typeof i != 'function')
							paren[i] = tmp[i];
					var qtt = "select etebar,fname,lname,min_etebar from naghshe.parvande left join naghshe.user_parvande on (user_parvande.parvande_id=parvande.id) where user_parvande.user_id = "+paren.user_id;
					if(paren.company_id==2)
						qtt = "select etebar,fname,lname,min_etebar from darma.profile left join "+poolDB+".user on (user.id=user_id) where user_id = "+paren.user_id;
					con.query(qtt,function(err,rows){
						if(err)
							paren.err = err;
						else if(rows.length > 0)
						{
							paren.etebar = rows[0].etebar;
							paren.fname = rows[0].fname;
							paren.lname = rows[0].lname;
							paren.min_etebar = rows[0].min_etebar;
						}
						else
							paren.etebar = 0;
						if(typeof fn == 'function')
							fn(paren);
						
					});
				}
				else
				{
					for(i in def)
						if(typeof i != 'function')
							paren[i] = def[i];
					if(typeof fn == 'function')
						fn(paren);
				}
			});
		}

	};
	this.changePass = function (newPass,fn)
	{
		var paren = this;
		this.connection.query("update cards set passr = '"+newPass+"' , pass = '"+md5(newPass)+"' where id = "+paren.id,function(err,rows){
			if(typeof fn == 'function')
				fn(err);
		});
	};
	this.add = function (terminal,funcCode,socket,dataObj)
	{
		var paren = this;
		var cards = paren;
		cards.rfid = dataObj[0];
		cards.pass=md5(dataObj[1]);
		cards.passr=dataObj[1];
		cards.en=0;
		var dtt =new Date();
		cards.tarikh_sodor = dtt.getFullYear()+"-"+(dtt.getMonth()+1)+"-"+dtt.getDate()+" "+dtt.getHours()+":"+dtt.getMinutes()+":"+dtt.getSeconds();
		cards.expire_date = "0000-00-00 00:00:00";
		cards.wrong_date ="0000-00-00 00:00:00"; 
		cards.shomare = dataObj[3];
		cards.id =-1;
		paren = cards;
		var con = this.connection;
		this.connection.query("select company_id from "+poolDB+".user where id = "+terminal.user_id,function(err,rows){
			if(err)
			{
				paren.err = err;
				if(typeof fn == 'function')
	                                fn(paren);
			}
			else
			{
				paren.company_id = rows[0].company_id;
				con.query("insert into cards (`rfid`, `user_id`, `tarikh_sodor`, `shomare`, `expire_date`, `pass`, `passr`, `wrong_count`, `wrong_date`, `en`,`company_id`) values ('"+paren.rfid+"',"+paren.user_id+",'"+paren.tarikh_sodor+"','"+paren.shomare+"','"+paren.expire_date+"','"+paren.pass+"','"+paren.passr+"',"+paren.wrong_count+",'"+paren.wrong_date+"',"+paren.en+","+paren.company_id+")",function(err,rows){
					if(err)
					{
						cards.err = err;
						stat=4;
						addLog(terminal.user_id,"درخواست ثبت کارت جدید خطای سرور","cards add unsuccessfull ,cards.construct "+JSON.stringify(err)+",stat="+stat);
						resToCl(terminal.id,funcCode,socket,stat,dataObj[0]);
					}
					else
					{
						cards.id = rows.insertId;
						stat=1;
						addLog(terminal.user_id,"درخواست ثبت کارت جدید به RFID "+cards.rfid+" با موفقیت انجام شد","cards add successfull id="+cards.id+" ,rfid="+cards.rfid+",stat="+stat);
						resToCl(terminal.id,funcCode,socket,stat,dataObj[0]);
					}
					if(typeof fn == 'function')
						fn(paren);
				});
			}
		});
	};
	this.delete = function (fn)
        {
		var paren = this;
		this.connection.query("delete from cards where id = "+paren.id,function(err,rows){
			if(err)
				paren.err = err;
			else
				paren.err = null;
			if(typeof fn == 'function')
                                fn(paren);
		});
	}
/*
	this.update = function (fn)
	{
		var paren = this;
		this.connection.query("update cards set `rfid` = "+paren.rfid+", `user_id` = "+paren.user_id+", `tarikh_sodor` = '"+paren.tarikh_sodor+"', `shomare` = '"+paren.shomare+"', `last_trans` = "+paren.last_trans+", `expire_date` = '"+paren.expire_date+"', `pass` = '"+paren.pass+"', `passr` = '"+paren.passr+"', `wrong_count` = "+paren.wrong_count+", `wrong_date` = '"+paren.wrong_date+"', `en` = "+paren.en+" where `id` = "+paren.id,function(err,rows){

			if(err)
				paren.err = err;
			else
				paren.id = rows.insertId;
			if(typeof fn == 'function')
				fn(paren);
		});
	};
*/
	this.terminalAvailable = function(terminal_id,fn)
	{
		var paren = this;
		if(paren.id > 0)
		{
			var con = this.connection;
			this.connection.query("select id from card_terminal where cards_id = "+paren.id,function(err,rows){
				if(err)
				{
					if(typeof fn == 'function')
						fn(false,err);
				}
				else
				{
					if(rows.lenght == 0)
					{
						if(typeof fn == 'function')
							fn(true,null);
					}
					else
						con.query("select id from card_terminal where terminal_id = "+terminal_id+" and cards_id = "+paren.id,function(err,rows){
							if(err)
							{
								if(typeof fn == 'function')
									fn(false,err);
							}
							else
								if(typeof fn == 'function')
									fn(true,null);
						});
				}
			});
		}
	};
	this.addMinLimit = function(dEtebar,fn)
	{
		var paren = this;
		var con = this.connection;
		var etTable = "naghshe.parvande";
		if(paren.company_id==2)
			etTable = "darma.profile";
		con.query("update "+etTable+" set min_etebar = min_etebar - "+dEtebar+" where user_id = "+paren.user_id,function(err,rows){
			if(typeof fn == 'function')
				fn(err);
		});
	};
	this.decEtebar = function(dEtebar,isEnteghal,fn)
	{
		var paren = this;
                if(paren.id > 0)
                {
                        var con = this.connection;
			var qtt = "update naghshe.parvande set etebar = etebar "+((isEnteghal === true)?" + ":" - ")+dEtebar+" where id in (select parvande_id from naghshe.user_parvande where user_id = "+paren.user_id+")";
			if(paren.company_id==2)
				qtt = "update darma.profile set etebar = etebar "+((isEnteghal === true)?" + ":" - ")+dEtebar+" where user_id = "+paren.user_id;
			con.query(qtt,function(err,rows){
				if(err)
				{
					if(typeof fn == 'function')
						fn(err);
				}
				else
				{
					var qqr = (typeof isEnteghal != 'undefined' && typeof isEnteghal != 'function' && isEnteghal === false)?"rooz_mablagh = rooz_mablagh + "+dEtebar:"rooz_enteghal = rooz_enteghal + "+dEtebar;
					con.query("update cards set "+qqr+" where id = "+paren.id,function(err,rows){
						var out=null;
						if(err)
							out = err
						if(typeof fn == 'function')
							fn(out);
					});
				}
			});
		}
	};
	this.check_mojoodi=function (socket,terminal_id,funcCode,tarikh,mablagh,isEnteghal,fn)
	{
		var ou = false;
		var cards_id = this.id;
		var con = this.connection;
		var cc = this;
		var proc = isEnteghal?poolDB+".ps_check_saghf_enteghal":poolDB+".ps_check_saghf_kharid";
		var contin = false;
		if(parseInt(cc.saghf_hadie,10)<0)
			contin=true;
		else if( (parseInt(cc.saghf_hadie)-parseInt(mablagh,10))>=0 )
			contin=true;

		if(contin)
		{
			console.log("call "+proc+"("+cards_id+",'"+tarikh+"',"+mablagh+")");
			con.query("call "+proc+"("+cards_id+",'"+tarikh+"',"+mablagh+",@a)",function(err,rows){
				if(err)
				{
					stat =17;
					addLog(cc.user_id,"خطای دیتا بیس","procedure "+proc+" faild to run" ,"stat="+stat);
					resToCl(terminal_id,funcCode,socket,stat,cc.id);
				}
				else
				{
					con.query("select @a",function(er,res){
						if(parseInt(res[0]['@a'],10)==1)
						{
							ou = true;
							if(typeof fn=='function')
								fn(ou);
						}
						else
						{
							stat =15;
							var st = isEnteghal?"انتقال":"خرید";
							var st1 = isEnteghal?"enteghal":"kharid";
							addLog(cc.user_id,"خطای سقف "+st+" روزانه از کارت شماره "+cc.shomare,st1+" unsuccessfull ,saghf "+st1+",stat="+stat);
							resToCl(terminal_id,funcCode,socket,stat,cc.id);
						}	
					});
				}
			});
		}
		else
		{
			stat =16;
			addLog(cc.user_id,"خطای سقف هدیه از کارت شماره سقف="+cc.saghf_hadie+" درخواست="+mablagh+" "+cc.shomare,"hadie unsuccessfull ,saghf hadie","stat="+stat);
			resToCl(terminal_id,funcCode,socket,stat,cc.id);
		}
	};
	this.check_fn =	function (socket,dataObj,funcCode,terminal_id,diftime,checketebar,fn)
	{
		var card = this;
		var nw = new Date();
		var dt = Date.parse(dataObj[dataObj.length-1]);
		var df1 = nw.getTime()-dt;
		var etebarTmp = (card.saghf_hadie<0)?card.etebar-card.min_etebar:card.etebar;
		var out = (card.id>0 && card.en==1 && card.pass==dataObj[1] && (nw.getTime()-dt)<diftime && (etebarTmp>=dataObj[dataObj.length-2] || !checketebar));
		var stat = 4;
		if(!out)
		{
			if(card.id<=0)
			{
				stat=2;
				addLog(card.user_id,"درخواست خرید خطا بعلت عدم وجود کارت کاربر","kharid unsuccessfull ,cards.id is less then 0,stat="+stat);
			}
			else if(card.en!=1)
			{
				stat=6;
				addLog(card.user_id,"درخواست خرید خطا بعلت عدم فعال بودن کاربر","kharid unsuccessfull ,en is 0,stat="+stat);
			}
			else if(card.pass!=dataObj[1])
			{
				stat=2;
				addLog(card.user_id,"درخواست خرید خطا بعلت رمز اشتباه","kharid unsuccessfull ,wrong password,stat="+stat);
			}
			else if(nw.getTime()-dt>=diftime)
			{
				stat = 5;
				addLog(card.user_id,"درخواست خرید خطا بعلت عدم تطابق تاریخ و زمان","kharid unsuccessfull ,time dif err ,stat="+stat);
			}
			else if(etebarTmp<dataObj[2] && checketebar)
			{
				stat = 3;
				addLog(card.user_id,"خطای کمبود اعتبار","kharid unsuccessfull ,etebar is not enough ,stat="+stat);
			}
			else
				addLog(card.user_id,"خطای اطلاعات ","kharid unsuccessfull ,etebar is not enough ,stat="+stat);
			resToCl(terminal_id,funcCode,socket,stat,dataObj[0]);
		}
		if(typeof fn == 'function')
			fn(out);
	};
}

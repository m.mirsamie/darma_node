var mysql = require('mysql');
var dirSep = "/";
var mainPath = loadpath();
var getPool = require(mainPath+'/tcp_new.js').getPool;
var poolDB = getPool();
module.exports.terminals_class = terminals_class;
function loadpath()
{
	var out = '';
	var mainPath = __dirname;
	out = mainPath.replace(/\//g,"|");
	out = out.replace(/\\/g,"|");
	var tmp = String(out).split("|");
	var out = "";
	for(var i = 0;i < tmp.length;i++)
		if(tmp[i]!='')
			if((i == tmp.length-1 && tmp[i]!='class')||(i < tmp.length-1))
				out += ((out!='' || mainPath.charAt(0)==dirSep)?dirSep:'')+tmp[i];
	return out;
}


function terminals_class(){
	this.id=-1;
	this.code=-1;
	this.last_ip='';
	this.first_trans_id=-1;
	this.last_trans_id=-1;
	this.user_id=-1;
	this.expire_date;
	this.jam_kol=0;
	this.can_register=0;
	this.en=0;
	this.fname="";
	this.lname="";
	this.err;
	this.construct = function (id,connection,fn)//(id,inHost,inPort,inDatabase,inUser,inPassword,fn)
	{
		this.connection = mysql.createConnection({
			host : connection.config.host,//'localhost',
			port : connection.config.port,//3306,
			database: poolDB,//'behdani',
			user : connection.config.user,//'root',
			password : connection.config.password,//'123456'
		});
		var paren = this;
		var thi = {};
		thi.id=-1;
		thi.code=-1;
		thi.last_ip='';
		thi.first_trans_id=-1;
		thi.last_trans_id=-1;
		thi.user_id=-1;
		thi.expire_date;
		thi.jam_kol=0;
		thi.can_register=0;
		thi.en=0;
		thi.fname="";
		thi.lname="";
		thi.err;
		if(typeof id != 'undefined')
		{
			this.connection.query("select `terminals`.`id`, `code`, `last_ip`, `first_trans_id`, `last_trans_id`, `user_id`, `expire_date`, `jam_kol`, `can_register`, `terminals`.`en`,`fname`,`lname` from terminals left join user on (user.id=user_id) where terminals.id="+id,function(err,rows){
				if(err)
					paren.err = err;
				else
				{
					if(rows.length>0)
						var tmp = rows[0];
					else
						var tmp = thi;
					for(i in tmp)
						if(typeof i != 'function')
							paren[i] = tmp[i];
				}
				if(typeof fn == 'function')
					fn(paren);
			});
		}
	};
	this.add = function (fn)
	{
		var paren = this;
		this.connection.query("insert into terminals (`code`, `last_ip`, `first_trans_id`, `last_trans_id`, `user_id`, `expire_date`, `jam_kol`, `can_register`, `en`) values ("+paren.code+",'"+paren.last_ip+"',"+paren.first_trans_id+","+paren.last_trans_id+","+paren.user_id+",'"+paren.expire_date+"',"+paren.jam_kol+","+paren.can_register+","+paren.en+")",function(err,rows){
			if(err)
				paren.err = err;
			else
				paren.id = rows.insertId;
			if(typeof fn == 'function')
				fn(paren);
		});
	};
	this.delete = function (fn)
        {
		var paren = this;
		this.connection.query("delete from terminals where id = "+paren.id,function(err,rows){
			if(err)
				paren.err = err;
			else
				paren.err = null;
			if(typeof fn == 'function')
                                fn(paren);
		});
	}
	this.update = function (fn)
	{
		var paren = this;
		this.connection.query("update terminals set `code` = "+paren.code+", `last_ip` = '"+paren.last_ip+"', `first_trans_id` = "+paren.first_trans_id+", `last_trans_id` = "+paren.last_trans_id+", `user_id` = "+paren.user_id+", `expire_date` = '"+paren.expire_date+"', `jam_kol` = "+paren.jam_kol+", `can_register` = "+paren.can_register+", `en` = "+paren.en+"  where `id` = "+paren.id,function(err,rows){

			if(err)
				paren.err = err;
			else
				paren.id = rows.insertId;
			if(typeof fn == 'function')
				fn(paren);
		});
	};
}

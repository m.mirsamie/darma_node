var dirSep = "/";
var mainPath = loadpath();//'/home/mehrdad_mahsa/projects/node/behdani';
//var mainPath = '/root/behdani';
var resToCl = require(mainPath+'/tcp_new.js').resToCl;
var addLog = require(mainPath+'/tcp_new.js').addLog;
var mysql = require('mysql');
var getPool = require(mainPath+'/tcp_new.js').getPool;
var poolDB = getPool();
module.exports.transactions_class = transactions_class;
function loadpath()
{
	var out = '';
	var mainPath = __dirname;
	//mainPath = 'c:\\ali\\gholi\\class';
	out = mainPath.replace(/\//g,"|");
	out = out.replace(/\\/g,"|");
	var tmp = String(out).split("|");
	var out = "";
	for(var i = 0;i < tmp.length;i++)
		if(tmp[i]!='')
			if((i == tmp.length-1 && tmp[i]!='class')||(i < tmp.length-1))
				out += ((out!='' || mainPath.charAt(0)==dirSep)?dirSep:'')+tmp[i];
	return out;
}
function transactions_class(){
        this.id=-1;
        this.cards_id=-1;
        this.tarikh;
        this.function_id=-1;
        this.mablagh=0;
        this.ter_id=-1;
        this.status=0;
        this.typ=0;
        this.err;
        this.construct = function (id,connection,fn)//(id,inHost,inPort,inDatabase,inUser,inPassword,fn)
        {
		this.connection = mysql.createConnection({
			host : connection.config.host,//'localhost',
			port : connection.config.port,//3306,
			database: poolDB,//'behdani',
			user : connection.config.user,//'root',
			password : connection.config.password,//'123456'
		});
		if(typeof fn == 'function')
			fn(this);
	};
	this.add = function(fn){
		var paren = this;
		this.connection.query("insert into transactions (`cards_id`, `tarikh`, `function_id`, `mablagh`, `ter_id`, `status`, `typ`) values ("+paren.cards_id+",'"+paren.tarikh+"',"+paren.function_id+","+paren.mablagh+","+paren.ter_id+","+paren.status+","+paren.typ+")",function(err,rows){
			if(err)
			{
				paren.id = -1;
				paren.err = err;
				if(typeof fn == 'function')
					fn(paren);
			}
			else
			{
				paren.id = rows.insertId;
				if(typeof fn == 'function')
					fn(paren);
			}
		});
	};
	//----------------------------------------------------------------------------
	this.transaction_fn=function(socket,connection,terminal_id,funcCode,dataObj,cards,check_etebar,fn)
	{
		var tran = this;
		var user_id = cards.user_id;
		var out = false;
		tran.construct(parseInt(dataObj[0],10),connection,function(inp){
			tran = inp;
			if(inp.err)
			{
				out = false;
				if(typeof fn=='function')
					fn(out);
			}
			else
			{
				tran.cards_id=cards.id;
				var dtt =new Date();
				tran.tarikh= dtt.getFullYear()+"-"+(dtt.getMonth()+1)+"-"+dtt.getDate()+" "+dtt.getHours()+":"+dtt.getMinutes()+":"+dtt.getSeconds();
				tran.function_id=funcCode;
				tran.mablagh = check_etebar?dataObj[2]:0;
				tran.ter_id = terminal_id;
				tran.status= 1;
				tran.typ = 2;
				tran.add(function(inp){
					tran = inp;
					if(tran.err)
					{
						out = false;
						addLog(user_id,"درخواست خرید مشکل در ثبت ترانس اکشن","transaction add unsuccessfull ,tran.add");
						stat=4;
						resToCl(terminal_id,funcCode,socket,stat,dataObj[0]);
					}
					else
						out = true;
					if(typeof fn=='function')
						fn(out);
				});
			} 
		});
	};
}


process.env.TZ = 'Asia/Tehran';
module.exports.resToCl = resToCl;
module.exports.addLog = addLog;
module.exports.getPool = getPool;
module.exports.getNaghshe = getNaghshe;
var kharid = 1;
var mojoodi =2;
var jadid = 3 ;
var info = 4 ;
var enteghal = 5;
var changePass = 6;
var tenTransactions = 7;
var sport =43004;


var host = "localhost";
var port = 3306;
var db = 'darma_sale';
var user = 'root';
var pass = '123456';
/*
var host = "192.168.1.11";
var port = 3306;
var db = 'behdani';
var user = 'root';
var pass = '123456';
*/
var net = require('net');
var mysql = require('mysql');
var connection = mysql.createConnection({
	host : host,//'localhost',
	port : port,//3306,
	database: db,//'behdani',
	user : user,//'root',
	password : pass,//'123456'
});
var cards_class = require(__dirname+'/class/cards_class.js').cards_class;
var terminals_class = require(__dirname+'/class/terminals_class.js').terminals_class;
var transactions_class = require(__dirname+'/class/transactions_class.js').transactions_class;
var md5 = require(__dirname+'/class/md5_class.js').md5;
var user_etebar_class = require(__dirname+'/class/user_etebar_class.js').user_etebar_class;
var log_class = require(__dirname+'/class/log_class.js').log_class;
var cards = new cards_class();
var terminals = new terminals_class();
var tran = new transactions_class();
var user_etebar = new user_etebar_class();
var logger = new log_class();
logger.construct(null,connection,null);
var diftime = 120*1000;
function getPool()
{
	var poolDB = "darma_pool";
	return poolDB;
}
function getNaghshe()
{
        var poolNaghshe = "darma_parvande";
        return poolNaghshe;
}
//----------------------------------------------------------------------------
function intToWord(inp)
{
	var out = [];
	out.push(inp%256);
	out.push((inp-(inp%256))/256);
	return(out);
}
//----------------------------------------------------------------------------
function getUTF8Length(str) {
    var utf8length = 0;
    str=String(str);
    if(str)
	    for (var n = 0; n < str.length; n++) {
		var c = str.charCodeAt(n);
		if (c < 128) {
		    utf8length++;
		}
		else //if((c > 127) && (c < 2048)) {
		    utf8length = utf8length+2;
		//}
	    }
    return utf8length;
}
//----------------------------------------------------------------------------
function resToCl(id,funcCode,socket,statJoin,user_id,etebar,json_obj)
{
	var dt = new Date();
	var tstat = String(statJoin).split(',');
	var stat = tstat[0];
	var piegiri = (tstat.length==2)?tstat[1]:-1;
	var fname = (tstat.length==3)?tstat[1]:-1;
	var lname = (tstat.length==3)?tstat[2]:-1;
	if(typeof json_obj != 'undefined' && funcCode == tenTransactions)
	{
		var dataObj=[
			user_id,
			stat,
			json_obj,
			dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds()
		];
	}
	else if(typeof etebar=='undefined' )
	{
		if(fname!=-1)
		{
			var dataObj=[
				user_id,
				stat,
				fname,
				lname,
				dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds()
			];
		}
		else
		{
			var dataObj=[
				user_id,
				stat,
				piegiri,
				dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds()
			];
		}
	}
	else
	{
		var dataObj=[
			user_id,
			stat,
			etebar,
			dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds()
		];
	}
	dataStr = JSON.stringify(dataObj);
        datalength = getUTF8Length(dataStr);
        var tout =[intToWord(id)[0],intToWord(id)[1],funcCode,intToWord(datalength)[0],intToWord(datalength)[1]];
        var bb = new Buffer(tout);
        var nb = new Buffer(dataStr,"utf-8");
        bb = Buffer.concat([bb,nb]);
        if(bb.length>0)
                socket.write(bb);
}
//----------------------------------------------------------------------------
function addLog(user_id,toz,extra)
{
	
	var dt = new Date();
	var regdate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
	var page = 'node,tcp.js';
	logger.page = page;
	logger.regdate = regdate;
	logger.user_id = user_id;
	logger.toz = toz;
	logger.extra = extra;
	logger.add();
}
//----------------------------------------------------------------------------
function kharid_fn(socket,connection,terminal_id,dataObj)
{
	cards.construct(dataObj[0],connection,function(inp){
		cards = inp;
		var check_etebar = true;
		var typ=-1;//reduce 
		cards.check_fn(socket,dataObj,kharid,terminal_id,diftime,check_etebar,function(out){
			if(out)
			{
				var isEnteghal=false;
				cards.check_mojoodi(socket,terminal_id,kharid,dataObj[3],dataObj[2],isEnteghal,function(out){
					if(out)
					{
						tran.transaction_fn(socket,connection,terminal_id,kharid,dataObj,cards,check_etebar,function(out){
							if(out)
							{
								user_etebar.construct(null,connection,function(inp){
									user_etebar = inp;
									user_etebar.user_etebar_fn(cards,dataObj,kharid,terminals,tran,typ,socket,isEnteghal);
								});
						
							}
						});
					}
				});
					
			}
		});
	});
}
//----------------------------------------------------------------------------
function mojoodi_fn(socket,connection,terminal_id,dataObj)
{
	cards.construct(dataObj[0],connection,function(inp){
		cards = inp;
		var check_etebar = false;
		cards.check_fn(socket,dataObj,mojoodi,terminal_id,diftime,check_etebar,function(out){
			if(out)
			{
				stat=1;
				addLog(cards.user_id,"درخواست موجودی با موفقیت انجام شد","mojoodi successfull ,all done mojoodi,stat="+stat);
				var etebarTmp = (cards.saghf_hadie<0)?cards.etebar-cards.min_etebar:cards.saghf_hadie;
				resToCl(terminal_id,mojoodi,socket,stat,dataObj[0],etebarTmp);
			}
		});
	});
}
function tenTrans_fn(socket,connection,terminal_id,dataObj)
{
        cards.construct(dataObj[0],connection,function(inp){
                cards = inp;
		var tr = new user_etebar_class();
		tr.history_trans(connection,cards.id,10,function(out){
			stat=1;
			addLog(cards.user_id,"ﺩﺮﺧﻭﺎﺴﺗ ده گردش آخر","ten transactions ,all done ten transactions,stat="+stat);
			resToCl(terminal_id,tenTransactions,socket,stat,dataObj[0],0,out);
			
		});
        });
}
//----------------------------------------------------------------------------
function jadid_fn(socket,connection,terminals,dataObj)
{
	if(dataObj.length==4)
	{
		if(parseInt(terminals.can_register,10)==1)
		{
			cards.construct(dataObj[0],connection,function(inp){
				cards = inp;
				if(inp.err)
				{
					stat=4;
					addLog(terminals.user_id,"درخواست ثبت کارت جدید با خطا مواجه شد بعلت تکراری بودن آیدی کارت","terminals unsuccessfull ,all done terminal,stat="+stat);
					resToCl(terminals.id,jadid,socket,stat,dataObj[0]);
				}
				else		
					cards.add(terminals,jadid,socket,dataObj);
			});
		}
		else
		{
			stat=6;
			addLog(terminals.user_id,"درخواست ثبت کارت جدید به  خطای عدم فعال بودن ترمینال","terminals unsuccessfull ,en is 0,stat="+stat);
			resToCl(terminals.id,jadid,socket,stat,dataObj[0]);
		}
	}
	else
	{
		stat=8;
		addLog(terminals.user_id,"درخواست ثبت کارت جدید خطای عدم فعال تطابق تعدادآرایه اطلاعات با در خواست مورد نظر","terminals unsuccessfull ,dataObj.length!=4,stat="+stat);
		resToCl(terminals.id,jadid,socket,stat,dataObj[0]);
	}
}
//----------------------------------------------------------------------------
function info_fn(socket,connection,terminal_id,dataObj)
{
	if(dataObj.length==2)
	{
		var dest_card = new cards_class();
		dest_card.loadByShomare(dataObj[0],connection,function(inp){
			dest_card=inp;
			if(inp.err)
			{
				stat=10;
				addLog(terminal_id,"درخواست اطلاعات ، خطا بعلت  کارت نامعتبر شماره "+dataObj[0],"info unsuccessfull ,shomare="+dataObj[0]+",stat="+stat);
				resToCl(terminal_id,info,socket,stat,-1);
			}
			else if(dest_card.en==1)
			{
				stat = "1,"+dest_card.fname+","+dest_card.lname;
				addLog(terminal_id,"درخواست اطلاعات ، کارت شماره "+dataObj[0]+" به نام "+dest_card.fname+" "+dest_card.lname,"info successfull ,shomare="+dataObj[0]+",stat="+stat);
				resToCl(terminal_id,info,socket,stat,dest_card.rfid);
			}
			else
			{
				stat = 13;
				addLog(terminal_id,"درخواست اطلاعات ، خطا بعلت غیر فعال بودن کارت مقصد ","info unsuccessfull ,shomare="+dataObj[0]+",stat="+stat);
				resToCl(terminal_id,info,socket,stat,dest_card.rfid);
			}
		});
	}
	else
	{
		stat=8;
		addLog(terminals.user_id,"درخواست اطلاعات بعلت عدم تطابق آرایه ورودی نا معتبر است","info unsuccessfull ,dataObj.length!=2,stat="+stat);
		resToCl(terminal_id,jadid,socket,stat,dataObj[0]);
	}
}
//----------------------------------------------------------------------------
function enteghal_fn(socket,connection,terminals,dataObj)
{
	cards.construct(dataObj[0],connection,function(inp){
		cards = inp;
		var tObj=[];
		for(var k=0;k<dataObj.length-1;k++)
			tObj.push(dataObj[k]);
		if(cards.saghf_hadie>=0)
		{
			stat = 19;
			addLog(terminals.user_id,"عدم امکان انتقال به علت هدیه بودن کارت مبدا","card is hadie no transfer,stat="+stat);
	                resToCl(terminal_id,jadid,socket,stat,dataObj[0]);
		}
		else
		{
			var check_etebar = true;
			cards.check_fn(socket,tObj,enteghal,terminal_id,diftime,check_etebar,function(out){
				if(out)
				{
					var isEnteghal=true;
					cards.check_mojoodi(socket,terminal_id,enteghal,dataObj[3],dataObj[2],isEnteghal,function(out){ //---check saghf enteghal && set rooz_enteghal to zero if needed
						var dest_card = new cards_class();
						dest_card.loadByShomare(parseInt(dataObj[4],10),connection,function(inp){
							dest_card = inp;
							if(dest_card.saghf_hadie>=0)
							{
								stat = 20;
								addLog(terminals.user_id,"عدم امکان انتقال به علت هدیه بودن کارت مقصد","card is hadie no transfer,stat="+stat);
								resToCl(terminal_id,jadid,socket,stat,dataObj[0]);
							}
							else
							{
								if(dest_card.id>0 && dest_card.en==1 && dest_card.user_id>0)
								{
									tran.transaction_fn(socket,connection,terminal_id,enteghal,dataObj,cards,check_etebar,function(out){
										if(out)
										{
											stat="1,"+tran.id;
											user_etebar.construct(null,connection,function(inp){
												user_etebar = inp;
												user_etebar.user_etebar_fn(dest_card,dataObj,enteghal,terminals,tran,1,socket,isEnteghal,function(out){
				if(out)
					user_etebar.user_etebar_fn(cards,dataObj,enteghal,terminals,tran,-2,socket,false);
			});
												addLog(cards.user_id,"درخواست انتقال وجه با موفقیت انجام گرفت کد پیگیری "+tran.id,"enteghal successfull ,all done enteghal,stat="+stat);
												//resToCl(terminals.id,enteghal,socket,stat,dataObj[0]);
											});
										}
									});
								}
								else if(dest_card.id<=0)
								{
									stat = 12;
									addLog(terminal_id,"درخواست اطلاعات ، خطا بعلت عدم وجود کارت مقصد ","info unsuccessfull ,shomare="+dataObj[0]+",stat="+stat);
									resToCl(terminals.id,enteghal,socket,stat,cards.rfid);
								}
								else if(dest_card.user_id<=0)
								{
									stat = 14;
									addLog(terminal_id,"درخواست اطلاعات ، خطا بعلت عدم اتصال کارت مقصد به کاربر ","info unsuccessfull ,shomare="+dataObj[0]+",stat="+stat);
									resToCl(terminals.id,enteghal,socket,stat,cards.rfid);
								}
								else if(dest_card.en==0)
								{
									stat = 13;
									addLog(terminal_id,"درخواست اطلاعات ، خطا بعلت غیر فعال بودن کارت مقصد ","info unsuccessfull ,shomare="+dataObj[0]+",stat="+stat);
									resToCl(terminals.id,enteghal,socket,stat,cards.rfid);
								}
							}
						});
					});
						
				}
			});
		}
	});
}
function changePass_fn(socket,connection,terminals,dataObj)
{
	cards.construct(dataObj[0],connection,function(inp){
		cards = inp;
		if(parseInt(cards.en,10)==1)
		{
			if(cards.pass==dataObj[1])
			{
				cards.changePass(dataObj[2],function(err){
					if(err)
					{
						stat = 4;
						addLog(terminals.id,"درخواست تغییر رمز کارت "+dataObj[0]+"، خطای سرور","change pass unsuccessfull "+err+" ,shomare="+dataObj[0]+",stat="+stat);
						resToCl(terminals.id,enteghal,socket,stat,cards.rfid);
					}
					else
					{
						stat = 1;
						addLog(terminals.id,"درخواست تغییر رمز کارت "+dataObj[0]+"،  با موفقیت انجام شد","change pass successfull ,shomare="+dataObj[0]+",stat="+stat);
						resToCl(terminals.id,enteghal,socket,stat,cards.rfid);
					}
				});
			}
			else
			{
				stat = 2;
				addLog(terminals.id,"درخواست تغییر رمز کارت "+dataObj[0]+"، خطا بعلت  ورود رمز اشتباه","change pass unsuccessfull pass wrong ,shomare="+dataObj[0]+",stat="+stat);
				resToCl(terminals.id,enteghal,socket,stat,cards.rfid);
			}
		}
		else
		{
			stat = 13;
			addLog(terminal_id,"درخواست تغییر رمز کارت ،  "+dataObj[0]+" خطا بعلت غیر فعال بودن کارت مقصد ","change pass unsuccessfull en=0 ,shomare="+dataObj[0]+",stat="+stat);
			resToCl(terminals.id,enteghal,socket,stat,cards.rfid);
		}
	});
}
//----------------------------------------------------------------------------
var server = net.createServer(function(socket){
	socket.on('data', function(data) {
		var id = -1;
		var funcCode = -1;
		var dataLength = 0;
		var inpData = '';
		var outData = '';
		var dataStr = '';
		var dataObj = [];
		if(data.length >= 5)
		{
			terminal_id = data[0]+data[1]*256;
			funcCode = data[2];
			dataLength = data[3]+data[4]*256;
			for(i=5;i<data.length;i++)
				inpData += String.fromCharCode(data[i]);
			dataObj = JSON.parse(inpData);
			terminals.construct(terminal_id,connection,function(inp){
				terminals = inp;
				if(terminals.id>0)
				{
					switch(funcCode)
					{
						case kharid:
							kharid_fn(socket,connection,terminal_id,dataObj);
							break;
						case mojoodi:
							mojoodi_fn(socket,connection,terminal_id,dataObj);
							break;
						case info:
							info_fn(socket,connection,terminal_id,dataObj);
							break;
						case enteghal:
							enteghal_fn(socket,connection,terminals,dataObj);
							break;
						case jadid:
							jadid_fn(socket,connection,terminals,dataObj);
							break;
						case changePass:
							changePass_fn(socket,connection,terminals,dataObj);
							break;
						case tenTransactions:
							tenTrans_fn(socket,connection,terminals,dataObj);
							break;
						default:
							stat = 11;
							addLog(cards.user_id,"عدم ارسال دستور مناسب","function code is invalid ,"+funcCode+",stat="+stat);
					}
				}
				else
				{
					stat=6;
					resToCl(id,funcCode,socket,stat,dataObj[0]);
					addLog(-1,"پایانه خرید موجود نیست","terminal.id less than 0");
				}
			});
		}
	}).on('connect', function() {
	}).on('end', function() {
	}).on('error',function(err){
		addLog(-1,"خطای سوکت در سرور",JSON.stringify(err));
	});
}).listen(sport).on('error',function(err){
	addLog(-1,"شروع به کار سرور با خطا مواجه گردید",JSON.stringify(err));
});
